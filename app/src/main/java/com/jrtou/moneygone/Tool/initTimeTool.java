package com.jrtou.moneygone.Tool;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Jrtou on 2017/2/28.
 */

/**
 * 時間抓取Tool
 */
public class InitTimeTool {
    private static final String TAG = "InitTimeTool";
    private Calendar mCalender;
    private SimpleDateFormat mFormatter;

    private String initDateStr;


    public InitTimeTool() {
        //初始化時間
        mCalender = Calendar.getInstance();

//        mFormatter = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss (E)", Locale.US);
        mFormatter = new SimpleDateFormat("yyyy/MM/dd", Locale.US);
        initDateStr = mFormatter.format(mCalender.getTime());
    }

    public String nextDate() {
        mCalender.add(Calendar.DATE, 1);
        initDateStr = mFormatter.format(mCalender.getTime());
        return initDateStr;
    }

    public String lastDate() {
        mCalender.add(Calendar.DATE, -1);
        initDateStr = mFormatter.format(mCalender.getTime());
        return initDateStr;
    }

    public String getToday() {
        mCalender = Calendar.getInstance();
        initDateStr = mFormatter.format(mCalender.getTime());
        return initDateStr;
    }

    public String setDateTime(int year, int month, int date) {
        try {
            Date st = mFormatter.parse(year + "/" + month + "/" + date);
            mCalender.setTime(st);
            initDateStr = mFormatter.format(mCalender.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return initDateStr;
    }

    public Calendar getCalendar() {
        return mCalender;
    }
}
