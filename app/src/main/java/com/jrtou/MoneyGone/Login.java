package com.jrtou.moneygone;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.jrtou.moneygone.Tool.SQLite.Realization.PersonalDao;

public class Login extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        findViews();
    }

    private void findViews() {
        Button mLoginBtn = (Button) findViewById(R.id.login_button);
        mLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                Bundle bundle = new Bundle();

                bundle.putString(PersonalDao.COLUMN_Account, "myAccount");
                bundle.putString(PersonalDao.COLUMN_PersonalName, "測試");
                bundle.putString(PersonalDao.COLUMN_Money, "NT");

                intent.putExtras(bundle);
                intent.setClass(Login.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
