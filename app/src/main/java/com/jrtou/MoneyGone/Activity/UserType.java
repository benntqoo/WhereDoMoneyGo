package com.jrtou.moneygone.Activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.jrtou.moneygone.Tool.Adapter.UseTypeItemBaseAdapter;
import com.jrtou.moneygone.Tool.SQLite.Realization.UseTypeDao;
import com.jrtou.moneygone.R;

public class UserType extends AppCompatActivity {
    // TODO: 2016/11/22 主業要傳帳號資訊近來
    private static final String TAG = "UserType";

    private UseTypeDao daoUseType;
    private Bundle mBundle;
    private static final String QUERY_SQL = "Select * From " + UseTypeDao.TABLE_NAME +
            " Where " + UseTypeDao.COLUMN_PERSONAL_ID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_type);
        findViews();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        daoUseType = new UseTypeDao(UserType.this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        daoUseType.close();
    }


    private void findViews() {
        mBundle = getIntent().getExtras();
        ListView mListView = (ListView) findViewById(R.id.use_type_listView);
        daoUseType = new UseTypeDao(UserType.this);
        UseTypeItemBaseAdapter mListViewAdapter = new UseTypeItemBaseAdapter(UserType.this, daoUseType.query
                (QUERY_SQL + " " + mBundle.getInt(UseTypeDao.COLUMN_PERSONAL_ID)));
        mListView.setAdapter(mListViewAdapter);

    }
}
