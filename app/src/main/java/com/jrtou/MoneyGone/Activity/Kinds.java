package com.jrtou.moneygone.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.jrtou.moneygone.R;
import com.jrtou.moneygone.Tool.Adapter.KindsViewPagerAdapter;

/**
 * 新增刪除後不會刷新adapter
 */
public class Kinds extends AppCompatActivity {
    private static final String TAG = "KindsActivity";

    private TabLayout mTab;
    private ViewPager mPager;
    private KindsViewPagerAdapter pagerAdapter;

    public static final String PAGE_INDEX = "PAGE_INDEX";
    private int pageIndex = 0;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kinds);
        Log.wtf(TAG, "onCreate");
        pageIndex = 0;
        findViews();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG, "onRestart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
        if (bundle != null) {
            mPager.setCurrentItem(bundle.getInt(KindsViewPagerAdapter.MONEY_TYPE));
        }
    }

    private void findViews() {
        mTab = (TabLayout) findViewById(R.id.tabLayout);
        mPager = (ViewPager) findViewById(R.id.viewPager);
        mTab.setupWithViewPager(mPager);

        pagerAdapter = new KindsViewPagerAdapter(this);
        mPager.setAdapter(pagerAdapter);
        mPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTab));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(TAG, "onActivityResult: " + requestCode);
        if (requestCode == -1) {
            bundle = getIntent().getExtras();
        } else {
            bundle = null;
        }
    }
}
