package com.jrtou.moneygone.Activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.jrtou.moneygone.R;
import com.jrtou.moneygone.Tool.Adapter.BudgetRecycleViewAdapter;
import com.jrtou.moneygone.Tool.SQLite.Realization.BudgetDao;

public class Budget extends AppCompatActivity {
    // 每個月10號自動新增
    private static final String TAG = "Budget";
    private static final String QUERY_SQL = "Select *  From " + TAG + " OrderBy desc";

    private BudgetDao daoBudget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_budget);
        findViews();
    }

    private void findViews() {
        daoBudget = new BudgetDao(Budget.this);
        TextView mYearMonth = null, mBudgetTxtItem = null, mCostTxtItem = null, mSurplusTxtItem = null;
        //Recycle view
        RecyclerView mRecycleView = (RecyclerView) findViewById(R.id.budgetRecycleView);
        BudgetRecycleViewAdapter mRecycleAdapter = new BudgetRecycleViewAdapter
                (daoBudget.getAll(), Budget.this, mYearMonth, mBudgetTxtItem, mCostTxtItem, mSurplusTxtItem);
        mRecycleView.setLayoutManager(new LinearLayoutManager(Budget.this));
        mRecycleView.setAdapter(mRecycleAdapter);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        daoBudget = new BudgetDao(Budget.this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        daoBudget.close();
    }
}
