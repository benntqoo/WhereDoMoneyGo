package com.jrtou.moneygone.Activity;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.jrtou.moneygone.R;
import com.jrtou.moneygone.Tool.Adapter.KindsRecycleViewAdapter;
import com.jrtou.moneygone.Tool.Adapter.KindsViewPagerAdapter;
import com.jrtou.moneygone.Tool.SQLite.Realization.KindsDao;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class KindsEditor extends AppCompatActivity {
    private static final String TAG = "KindsEditor";
    private List<String> ImageResource = new ArrayList<String>();

    private Bundle mBundle;
    private String toolBarTittle;//Toolbar tittle

    //dialog data
    private String tittle;
    private String okText;
    private String cancelText;
    private String message;
    private String cancelBtnText;

    /**
     * 0: 收入 1:支出
     */
    private int moneyType;
    /**
     * true:新增 false:修改
     */
    private Boolean activityMode;

    //UI
    private EditText kindsEditorEdit;
    private ImageView kindsEditorImage;

    private KindsRecycleViewAdapter mRecycleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kinds_editor);
        initData();
        findViews();
    }

    private void initData() {
        mBundle = getIntent().getExtras();
        activityMode = mBundle.getBoolean(KindsViewPagerAdapter.ACTIVITY_MODE);// true:新增 false:編輯
        moneyType = mBundle.getInt(KindsViewPagerAdapter.MONEY_TYPE);//0: 收入 1:支出

        tittle = getResources().getString(R.string.dialog_tittle);
        okText = getResources().getString(R.string.dialog_ok);
        cancelText = getResources().getString(R.string.dialog_cancel);

        if (activityMode) {
            toolBarTittle = getResources().getString(R.string.kinds_editor_insert_tittle);
            cancelBtnText = getResources().getString(R.string.dialog_cancel);
            message = getResources().getString(R.string.dialog_message_new);
        } else {
            toolBarTittle = getResources().getString(R.string.kinds_editor_edit_tittle);
            cancelBtnText = getResources().getString(R.string.dialog_delete);
            message = getResources().getString(R.string.dialog_message_edit);
        }

        if (moneyType == 0) {
            toolBarTittle = toolBarTittle + "-" + getResources().getString(R.string.kinds_editor_income);
        } else if (moneyType == 1) {
            toolBarTittle = toolBarTittle + "-" + getResources().getString(R.string.kinds_editor_expenses);
        }
    }

    private void findViews() {
        getIconFileName();

        kindsEditorImage = (ImageView) findViewById(R.id.kindsEditorImage);
        kindsEditorImage.setImageResource(mBundle.getInt(KindsDao.COLUMN_ImageResourceID));

        kindsEditorEdit = (EditText) findViewById(R.id.kindsEditorEdit);
        kindsEditorEdit.setText(mBundle.getString(KindsDao.COLUMN_KindsName));

        final ShowDialog dialog = new ShowDialog();

        //button ok
        Button kindsEditor_btnOk = (Button) findViewById(R.id.kindsEditor_btnOk);
        kindsEditor_btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doSQL();
            }
        });

        //button cancel
        Button kindsEditor_btnCancel = (Button) findViewById(R.id.kindsEditor_btnCancel);
        kindsEditor_btnCancel.setText(cancelBtnText);
        kindsEditor_btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activityMode) {
                    dialog.getCancelDialog().show();
                } else {
                    dialog.getDeleteDialog().show();

                }
            }
        });

        //toolbar
        Toolbar mToolBar = (Toolbar) findViewById(R.id.editorToolbar);
        mToolBar.setTitle(toolBarTittle);

        //Recycle View
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.kindsEditorRecycleView);
        mRecycleAdapter = new KindsRecycleViewAdapter(ImageResource, KindsEditor.this, kindsEditorImage);

        recyclerView.setLayoutManager(new GridLayoutManager(this, 6)); // 線性宮格顯示 類似 grid view
        recyclerView.setAdapter(mRecycleAdapter);
        mRecycleAdapter.initImageID(mBundle.getInt(KindsDao.COLUMN_ImageResourceID));

//        mRecycleView.setLayoutManager(new LinearLayoutManager(mDialog.getContext())); // 線性顯示 類似listView
//        mRecycleView.setLayoutManager(new StaggeredGridLayoutManager(2, OrientationHelper.VERTICAL));// 線性宮格顯示類似 瀑布流

    }

    private void getIconFileName() {
        Field[] mMipmap = R.mipmap.class.getFields();
        for (Field f : mMipmap) {
            if (f.getName().contains("item_")) {
                Log.d(TAG, "findViews: R.mipmap." + f.getName() + "\n");
                ImageResource.add(f.getName());
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            ShowDialog dialog = new ShowDialog();
            dialog.getCancelDialog().show();
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 新增 修改SQL
     */
    private void doSQL() {
        if (kindsEditorEdit.getText().toString().trim().length() >= 1) {
            KindsDao kindsDao = new KindsDao(KindsEditor.this);
            ContentValues cv = new ContentValues();
            if (activityMode) {
                cv.put(KindsDao.COLUMN_ImageResourceID, mRecycleAdapter.getImageID());
                cv.put(KindsDao.COLUMN_KindsName, kindsEditorEdit.getText().toString());
                cv.put(KindsDao.COLUMN_MoneyType, moneyType);

                if (kindsDao.insert(cv)) {
                    Toast.makeText(KindsEditor.this, R.string.success_insert, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(KindsEditor.this, R.string.exception_insert, Toast.LENGTH_SHORT)
                            .show();
                }
            } else {
                cv.put(KindsDao.COLUMN_KindsName, kindsEditorEdit.getText().toString());//item name
                cv.put(KindsDao.COLUMN_MoneyType, moneyType);
                cv.put(KindsDao.COLUMN_ImageResourceID, mRecycleAdapter.getImageID());
                cv.put(KindsDao.COLUMN_id, mBundle.getInt(KindsDao.COLUMN_id));

                if (kindsDao.modify(cv)) {
                    cv.clear();
                    kindsDao.close();
                    Toast.makeText(KindsEditor.this, getResources().getString(R.string.success_modify), Toast
                            .LENGTH_SHORT).show();
                } else {
                    cv.clear();
                    kindsDao.close();
                    Toast.makeText(KindsEditor.this, getResources().getString(R.string.exception_modify), Toast
                            .LENGTH_SHORT).show();
                }
            }
//            Intent backIntent = new Intent(KindsEditor.this, Kinds.class);
//            backIntent.putExtra(KindsViewPagerAdapter.MONEY_TYPE, moneyType);
//            setResult(-1, backIntent);

            /*
            backIntent.putExtra("finish", new ResultReceiver(null) {
                @Override
                protected void onReceiveResult(int resultCode, Bundle resultData) {
                    Log.i(TAG, "onReceiveResult");
                    KindsEditor.this.finish();
                    super.onReceiveResult(resultCode, resultData);
                }
            });
            startActivityForResult(backIntent, -1);
*/
            finish();
        } else {
            Toast.makeText(KindsEditor.this, getResources().getString(R.string.kinds_editor_insert_exception), Toast
                    .LENGTH_SHORT).show();
        }
    }

    /**
     * AlertDialog 提示
     */
    private class ShowDialog {
        private AlertDialog.Builder cancelDialog;
        private AlertDialog.Builder deleteDialog;

        public ShowDialog() {
            cancelDialog = new AlertDialog.Builder(KindsEditor.this);
            cancelDialog.setTitle(tittle).setMessage(message)
                    .setPositiveButton(okText, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    }).setNegativeButton(cancelText, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            if (!activityMode) {
                deleteDialog = new AlertDialog.Builder(KindsEditor.this);
                deleteDialog.setTitle(getResources().getString(R.string.dialog_delete)).setMessage
                        (getResources().getString
                                (R.string.dialog_message_delete)).setPositiveButton
                        (okText, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                KindsDao kindDao = new KindsDao(KindsEditor.this);

                                if (kindDao.delete(new String[]{String.valueOf(mBundle.get(KindsDao.COLUMN_id))})) {
                                    Toast.makeText(KindsEditor.this, getResources().getString(R.string
                                            .success_delete), Toast
                                            .LENGTH_SHORT)
                                            .show();
                                } else {
                                    Toast.makeText(KindsEditor.this, getResources().getString(R.string
                                            .exception_delete), Toast
                                            .LENGTH_SHORT).show();
                                }
                                finish();
                            }
                        }).setNegativeButton(cancelText, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
            }
        }

        public AlertDialog.Builder getCancelDialog() {
            return cancelDialog;
        }

        public AlertDialog.Builder getDeleteDialog() {
            return deleteDialog;
        }
    }
}
