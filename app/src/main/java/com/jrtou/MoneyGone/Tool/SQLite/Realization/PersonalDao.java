package com.jrtou.moneygone.Tool.SQLite.Realization;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.jrtou.moneygone.Tool.SQLite.DAO;
import com.jrtou.moneygone.Tool.SQLite.IDAO;

/**
 * Created by Administrator on 2016/11/21.
 *
 */

public class PersonalDao extends DAO implements IDAO {
    /*****************************
     * TABLE: 帳號表
     * CREATE: 2016/11/21
     * VERSION: 1
     * MEMO:
     *****************************/
    private static final String TAG = "PersonalDao";

    public static final String TABLE_NAME = "Personal";                               //表名

    public static final String COLUMN_id = "_id";
    public static final String COLUMN_PersonalName = "PersonalName";
    public static final String COLUMN_Money = "Money";
    public static final String COLUMN_Account = "Account";
    public static final String COLUMN_Password = "Password";
    public static final String COLUMN_Image = "Image";
    public static final String COLUMN_Def1 = "Def1";
    public static final String COLUMN_Def2 = "Def2";

    public static final String CREATE_Personal_Table = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
            COLUMN_id + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +            //pk
            COLUMN_PersonalName + " TEXT NOT NULL," +                               //戶名
            COLUMN_Money + " TEXT," +                                               //主要使用貨幣(預設台幣)
            COLUMN_Account + " TEXT NOT NULL," +                                    //帳號
            COLUMN_Password + " TEXT NOT NULL," +                                   //密碼(加密後)
            COLUMN_Image + " TEXT," +                                               //個人圖像
            COLUMN_Def1 + " TEXT," +
            COLUMN_Def2 + " TEXT);";

    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

    public PersonalDao(Context context) {
        super(context);
        initialDetail();
    }

    public PersonalDao(SQLiteDatabase sqLiteDatabase) {
        super(sqLiteDatabase);
        initialDetail();
    }

    @Override
    public void onDropTable() {

    }

    @Override
    public void onCreateTable() {

    }

    @Override
    public void onUpgradeTable() {

    }

    @Override
    public void onBuildDetail() {

    }

    @Override
    public void initialDetail() {
        setTableName(TABLE_NAME);
        setKeyId(COLUMN_id);
        setDropTable(DROP_TABLE);
        setCreateTable(CREATE_Personal_Table);
    }
}
