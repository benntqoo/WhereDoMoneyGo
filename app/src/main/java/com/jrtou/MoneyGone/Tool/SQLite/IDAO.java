package com.jrtou.moneygone.Tool.SQLite;

import android.content.ContentValues;

/**
 * Created by Administrator on 2016/11/7.
 *
 *
 */

public interface IDAO {
    void onDropTable();//刪除表

     void onCreateTable();//新增表

     void onUpgradeTable();//更新表

     void onBuildDetail();//建立基本資料

     void initialDetail();//初始資料


     boolean insert(ContentValues contentValues);//新增

     boolean modify(ContentValues contentValues);//修改

     boolean delete(String[] _id);//刪除
}
