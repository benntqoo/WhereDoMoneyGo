package com.jrtou.moneygone.Tool.Adapter;

import android.content.ContentValues;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jrtou.moneygone.Tool.SQLite.Realization.BudgetDao;
import com.jrtou.moneygone.R;

import java.util.List;

/**
 * Created by Administrator on 2016/11/19.
 */

public class BudgetRecycleViewAdapter extends RecyclerView.Adapter<BudgetRecycleViewAdapter.RelativeLayoutViewHolder> {
    private static final String TAG = "Budget RecycleView ItemClick";
    private List<ContentValues> mDataList;
    private Context mContext;
    private TextView mYearMonth, mBudgetTxtItem, mCostTxtItem, mSurplusTxtItem;

    public BudgetRecycleViewAdapter(List<ContentValues> mDataList, Context mContext, TextView mYearMonth, TextView
            mBudgetTxtItem, TextView mCostTxtItem, TextView mSurplusTxtItem) {
        this.mDataList = mDataList;
        this.mContext = mContext;
        this.mYearMonth = mYearMonth;
        this.mBudgetTxtItem = mBudgetTxtItem;
        this.mCostTxtItem = mCostTxtItem;
        this.mSurplusTxtItem = mSurplusTxtItem;
    }

    @Override
    public RelativeLayoutViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.budget_recycelview_item, parent, false);
        RelativeLayoutViewHolder budgetViewHolder = new RelativeLayoutViewHolder(v);
        return budgetViewHolder;
    }

    @Override
    public void onBindViewHolder(RelativeLayoutViewHolder holder, int position) {
        ContentValues cv = mDataList.get(position);
        holder.mYearMonth.setText(cv.getAsString(BudgetDao.COLUMN_YearMonth));
        if (cv.getAsInteger(BudgetDao.COLUMN_OtherAmount) != -1) {
            holder.mBudgetTxt.setText(cv.getAsInteger(BudgetDao.COLUMN_OtherAmount));
        } else {
            holder.mBudgetTxt.setText(String.valueOf(cv.getAsInteger(BudgetDao.COLUMN_Amount)));
        }

        holder.mCostTxt.setText(String.valueOf(cv.getAsFloat(BudgetDao.COLUMN_COST)));
        holder.mSurplusTxt.setText(String.valueOf(cv.getAsDouble(BudgetDao.COLUMN_SURPLUS)));
    }

    @Override
    public int getItemCount() {
        return mDataList.size();
    }

    public class RelativeLayoutViewHolder extends RecyclerView.ViewHolder {
        private TextView mYearMonth, mBudgetTxt, mCostTxt, mSurplusTxt;

        public RelativeLayoutViewHolder(View itemView) {
            super(itemView);
            this.mYearMonth = (TextView) itemView.findViewById(R.id.budgetTxtDateTime);
            this.mBudgetTxt = (TextView) itemView.findViewById(R.id.budgetTxtBudget);
            this.mCostTxt = (TextView) itemView.findViewById(R.id.budgetTxtCost);
            this.mSurplusTxt = (TextView) itemView.findViewById(R.id.budgetTxtSurplus);
        }
    }
}
