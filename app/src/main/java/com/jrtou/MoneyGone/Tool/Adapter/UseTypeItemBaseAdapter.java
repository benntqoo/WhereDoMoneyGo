package com.jrtou.moneygone.Tool.Adapter;

import android.content.ContentValues;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.jrtou.moneygone.Tool.SQLite.Realization.UseTypeDao;
import com.jrtou.moneygone.R;

import java.util.List;

/**
 * Created by Administrator on 2016/11/22.
 */

public class UseTypeItemBaseAdapter extends BaseAdapter {
    private static final String TAG = "UseTypeItemBaseAdapter";
    LayoutInflater mLayoutInflater;
    List<ContentValues> mList;

    public UseTypeItemBaseAdapter(Context mConText, List<ContentValues> mList) {
        this.mLayoutInflater = LayoutInflater.from(mConText);
        this.mList = mList;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mList.indexOf(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;

        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.usertype_listview_item, null);
            viewHolder = new ViewHolder((TextView) convertView.findViewById(R.id.use_Type_card_name),
                    (TextView) convertView.findViewById(R.id.use_type_last_store_value_date),
                    (TextView) convertView.findViewById(R.id.use_type_balance),
                    (TextView) convertView.findViewById(R.id.use_type_last_store_value));
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        ContentValues cv = (ContentValues) getItem(position);
        viewHolder.mTxtCardName.setText(cv.getAsString(UseTypeDao.COLUMN_UseTypeName));
        viewHolder.mTxtLastStoreValue.setText(cv.getAsInteger(UseTypeDao.COLUMN_LastStoreValue));
        viewHolder.mTxtLastStoreValueDate.setText(cv.getAsString(UseTypeDao.COLUMN_LastStoreValueDate));
        viewHolder.mTxtTypeBalance.setText(cv.getAsInteger(UseTypeDao.COLUMN_Balance));

        return convertView;
    }

    private class ViewHolder {
        TextView mTxtCardName, mTxtLastStoreValueDate, mTxtTypeBalance, mTxtLastStoreValue;

        public ViewHolder(TextView mTxtCardName, TextView mTxtLastStoreValueDate, TextView mTxtTypeBalance, TextView
                mTxtLastStoreValue) {
            this.mTxtCardName = mTxtCardName;
            this.mTxtLastStoreValueDate = mTxtLastStoreValueDate;
            this.mTxtTypeBalance = mTxtTypeBalance;
            this.mTxtLastStoreValue = mTxtLastStoreValue;
        }
    }
}
