package com.jrtou.moneygone.Tool.Adapter;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;

import com.jrtou.moneygone.Activity.KindsEditor;
import com.jrtou.moneygone.Tool.SQLite.Realization.KindsDao;
import com.jrtou.moneygone.R;

import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * Created by Jrtou on 2017/2/6.+
 */

public class KindsViewPagerAdapter extends PagerAdapter {
    private static final String Tag = "KindsViewPagerAdapter";
    private final String[] tabTittles = {"支出", "收入"};

    public static final String ACTIVITY_MODE = "activityMode";//true:新增 false:修改
    public static final String MONEY_TYPE = "moneyType";//0:支出 1:收入

    private kindsListViewAdapter adapter;
    private LayoutInflater mLayoutInflater;
    private Context context;
    private KindsDao dao;
    private List<ContentValues> list;
    private ImageButton imageButton;
    private ListView mListView;


    public KindsViewPagerAdapter(Context context) {
        this.context = context;
        dao = new KindsDao(context);
        mLayoutInflater = LayoutInflater.from(context);
        list = dao.query("Select * from " + KindsDao.TABLE_NAME + " Where " + KindsDao.COLUMN_MoneyType + "= 1");
        adapter = new kindsListViewAdapter(context, list);
    }

    //控制每一個子view
    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        Log.d(TAG, "setPrimaryItem: " + position + "  " + tabTittles[position]);
        String tittle = tabTittles[position];

        if (tittle.equals("收入")) {
            imageButton.setTag(0);
            list = dao.query("Select * from " + KindsDao.TABLE_NAME + " Where " + KindsDao.COLUMN_MoneyType + "= 0");
        } else if (tittle.equals("支出")) {
            imageButton.setTag(1);
            list = dao.query("Select * from " + KindsDao.TABLE_NAME + " Where " + KindsDao.COLUMN_MoneyType + "= 1");
        }

        adapter.refresh(list);
        mListView.setAdapter(adapter);
        super.setPrimaryItem(container, position, object);
    }

    @Override
    public int getCount() {
        Log.d(Tag, "getCount" + String.valueOf(tabTittles.length));
        return tabTittles.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        Log.d(Tag, "isViewFromObject-" + String.valueOf(view.getId()));
        return object == view;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Log.d(Tag, "getPageTitle-" + String.valueOf(position));
        return tabTittles[position];
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        super.destroyItem(container, position, object);
        Log.d(Tag, "destroyItem-");
        container.removeView((View) object);
        dao.close();
    }


    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        Log.d(Tag, "instantiateItem");
        View view = mLayoutInflater.inflate(R.layout.kinds_viewpager_view, container, false);
        container.addView(view);

        imageButton = (ImageButton) view.findViewById(R.id.kinds_ImageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle mBundle = new Bundle();
                mBundle.putString(KindsDao.COLUMN_KindsName, "");
                mBundle.putInt(KindsDao.COLUMN_ImageResourceID, R.mipmap.item_noodles);
                mBundle.putBoolean(ACTIVITY_MODE, true);//新增

                if (tabTittles[position].equals("支出")) {
                    mBundle.putInt(MONEY_TYPE, 1);//支出 / 收入
                } else {
                    mBundle.putInt(MONEY_TYPE, 0);
                }


                Intent intent = new Intent();
                intent.putExtras(mBundle);
                intent.setClass(context, KindsEditor.class);
                context.startActivity(intent);
            }
        });

        mListView = (ListView) view.findViewById(R.id.kinds_pager_list);
        mListView.setAdapter(adapter);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ContentValues cv = (ContentValues) parent.getItemAtPosition(position);
                Log.d(Tag, "onItemClick:" + cv.getAsString(KindsDao.COLUMN_KindsName));

                Bundle mBundle = new Bundle();
                mBundle.putString(KindsDao.COLUMN_KindsName, cv.getAsString(KindsDao.COLUMN_KindsName));//名稱
                mBundle.putInt(KindsDao.COLUMN_ImageResourceID, cv.getAsInteger(KindsDao.COLUMN_ImageResourceID));//圖片
                mBundle.putBoolean(ACTIVITY_MODE, false);//編輯
                mBundle.putInt(KindsDao.COLUMN_id, cv.getAsInteger(KindsDao.COLUMN_id));//修改帶入_id
                mBundle.putInt(MONEY_TYPE, (Integer) imageButton.getTag());//支出 / 收入

                Intent intent = new Intent();

                intent.putExtras(mBundle);
                intent.setClass(context, KindsEditor.class);
                context.startActivity(intent);
            }
        });
        return view;
    }
}
