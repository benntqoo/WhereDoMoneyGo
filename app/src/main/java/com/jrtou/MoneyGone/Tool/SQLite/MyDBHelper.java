package com.jrtou.moneygone.Tool.SQLite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.jrtou.moneygone.Tool.SQLite.Realization.BudgetDao;
import com.jrtou.moneygone.Tool.SQLite.Realization.KindsDao;

/**
 * Created by JrTou on 2016/10/25.
 */

public class MyDBHelper extends SQLiteOpenHelper {

    public static final String TAG = "MY_DB_HELPER";
    private static final String DATABASE_NAME = "whereDoMoneyGo.db"; //資料庫名稱
    public static final int DATABASE_VERSION = 1; //資料庫版本，資料結構改變的時候要更改這個數字，通常是加一
    private static SQLiteDatabase database; //資料庫物件，固定的欄位變數


    public MyDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.wtf(TAG, "MyDBHelper: Constructor");
    }

    // 建構子，在一般的應用都不需要修改 >> 指定資料庫檔名與版本，並初始化SQLiteOpenHelper類別
    public MyDBHelper(Context context, String DBName, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DBName, factory, version);
        Log.wtf(TAG, "MyDBHelper: Constructor");
    }


    // 需要資料庫的元件呼叫這個方法，這個方法在一般的應用都不需要修改
    public static SQLiteDatabase getDatabase(Context context) {
        Log.wtf(TAG, "MyDBHelper: getDatabase");
        if (database == null || !database.isOpen()) {
            database = new MyDBHelper(context, DATABASE_NAME, null, DATABASE_VERSION).getWritableDatabase();
        }
        return database;
    }

    @Override // 建立應用程式需要的表格
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.wtf(TAG, "onCreate: 建立資料庫 版本號:" + sqLiteDatabase.getVersion());
//        onSQLiteTool(sqLiteDatabase, 0, DATABASE_VERSION);
        KindsDao kindsDao = new KindsDao(sqLiteDatabase);
        kindsDao.onCreateTable();
        kindsDao.onBuildDetail();

        BudgetDao budgetDao = new BudgetDao(sqLiteDatabase);
        budgetDao.onCreateTable();
        budgetDao.onBuildDetail();
    }

    @Override // 資料庫升級
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        Log.wtf(TAG, " onUpgrade:  當前版本:" + oldVersion + " 更新後版本:" + newVersion);
        onSQLiteTool(sqLiteDatabase, oldVersion, DATABASE_VERSION);
    }

    @Override //資料庫降級
    public void onDowngrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        super.onDowngrade(sqLiteDatabase, oldVersion, newVersion);
        Log.wtf(TAG, "onDowngrade: 當前版本:" + oldVersion + " 降級後版本:" + newVersion);
        onSQLiteTool(sqLiteDatabase, oldVersion, newVersion);
    }

    private void onSQLiteTool(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.setVersion(newVersion);// 更換新版本
        if (oldVersion == 1) {
            KindsDao kindsDao = new KindsDao(sqLiteDatabase);
            kindsDao.onCreateTable();
            kindsDao.onBuildDetail();

        } else if (newVersion > 11) {
            Log.wtf(TAG, "onSQLiteTool: if newVersion > 11");
            KindsDao kindsDao = new KindsDao(sqLiteDatabase);
            kindsDao.onDropTable();
            kindsDao.onCreateTable();
            kindsDao.onBuildDetail();

            BudgetDao budgetDao = new BudgetDao(sqLiteDatabase);
            budgetDao.onDropTable();
            budgetDao.onCreateTable();
            budgetDao.onBuildDetail();

        }
    }

}




