package com.jrtou.moneygone.Tool.SQLite.Realization;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.jrtou.moneygone.Tool.SQLite.IDAO;
import com.jrtou.moneygone.Tool.SQLite.DAO;

/**
 * Created by Administrator on 2016/11/21.
 */

public class UseTypeDao extends DAO implements IDAO {
    /*****************************
     * TABLE: 消費方式
     * CREATE: 2016/11/07
     * VERSION: 1
     * MEMO:
     *****************************/
    private static final String TAG = "UseTypeDao";
    public static final String TABLE_NAME = "UseType";

    public static final String COLUMN_id = "_id";
    public static final String COLUMN_UseTypeName = "UseTypeName";
    public static final String COLUMN_Description = "Description";
    public static final String COLUMN_PERSONAL_ID = "PersonalID";
    public static final String COLUMN_Balance = "balance";
    public static final String COLUMN_LastStoreValue = "LastStoreValue";
    public static final String COLUMN_LastStoreValueDate = "LastStoreValueDate";
    public static final String COLUMN_Def1 = "Def1";
    public static final String COLUMN_Def2 = "Def2";

    public static final String CREATE_UseTypeTable = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
            COLUMN_id + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +              //Pk
            COLUMN_UseTypeName + " TEXT NOT NULL," +                                  //名稱
            COLUMN_Description + " TEXT," +                                           //備註
            COLUMN_PERSONAL_ID + " INTEGER NOT NULL," +                               //帳號PK
            COLUMN_Balance + "REAL NOT NULL," +                                       //餘額
            COLUMN_LastStoreValue + "REAL NOT NULL," +                                //最後儲值金額
            COLUMN_LastStoreValueDate + "TEXT(10) NOT NULL," +                        //最後儲值日期
            COLUMN_Def1 + " TEXT," +
            COLUMN_Def2 + " TEXT);";

    public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;

    public UseTypeDao(Context context) {
        super(context);
        initialDetail();
    }

    public UseTypeDao(SQLiteDatabase sqLiteDatabase) {
        super(sqLiteDatabase);
        initialDetail();
    }

    @Override
    public void onDropTable() {

    }

    @Override
    public void onCreateTable() {

    }

    @Override
    public void onUpgradeTable() {

    }

    @Override
    public void onBuildDetail() {

    }

    @Override
    public void initialDetail() {
        setCreateTable(CREATE_UseTypeTable);
        setDropTable(DROP_TABLE);
        setTableName(TABLE_NAME);
        setKeyId(COLUMN_id);
    }
}
