package com.jrtou.moneygone.Tool.ImageTool;

import com.jrtou.moneygone.R;

/**
 * Created by Jrtou on 2017/1/26.
 */

public class InitializeImage {
    //圖片版本
    private static final String IMAGE_VERSION = "1.0";
    //圖片位置
    public static final int[] images = {
            R.mipmap.item_airport, R.mipmap.item_aquarium, R.mipmap.item_bank, R.mipmap.item_bankcards,
            R.mipmap.item_bicycle, R.mipmap.item_bread, R.mipmap.item_briefcase, R.mipmap.item_bullish,
            R.mipmap.item_church, R.mipmap.item_cocktail, R.mipmap.item_coffee, R.mipmap.item_cup,
            R.mipmap.item_delete, R.mipmap.item_diningroom, R.mipmap.item_doghouse, R.mipmap.item_electricity,
            R.mipmap.item_film, R.mipmap.item_fork, R.mipmap.item_game, R.mipmap.item_gas,
            R.mipmap.item_gift, R.mipmap.item_hospital, R.mipmap.item_moneybag, R.mipmap.item_moneybox,
            R.mipmap.item_mrt, R.mipmap.item_parking, R.mipmap.item_phone, R.mipmap.item_pickup,
            R.mipmap.item_pill, R.mipmap.item_pizza, R.mipmap.item_restaurant, R.mipmap.item_running,
            R.mipmap.item_scooter, R.mipmap.item_shopping, R.mipmap.item_smoking, R.mipmap.item_strawberry,
            R.mipmap.item_strike, R.mipmap.item_stroller, R.mipmap.item_taxi, R.mipmap.item_tomato,
            R.mipmap.item_trunk, R.mipmap.item_tshit, R.mipmap.item_visa, R.mipmap.item_water
    };

    /**
     * @return image version
     */
    public static String getVersion() {
        return IMAGE_VERSION;
    }

    /**
     * @param index list index
     * @return image
     */
    public static int getImage(int index) {
        int imageIndex = index > getLength() ? 0 : index;
        return images[imageIndex];
    }

    /**
     * @return image count
     */
    public static int getLength() {
        return images.length;
    }
}
