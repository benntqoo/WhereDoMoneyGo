package com.jrtou.moneygone.Tool.Adapter;

import android.content.ContentValues;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jrtou.moneygone.R;
import com.jrtou.moneygone.Tool.SQLite.Realization.KindsDao;

import java.util.List;

/**
 * Created by Jrtou on 2017/2/7.
 */

public class kindsListViewAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private List<ContentValues> mList;


    public kindsListViewAdapter(Context context, List<ContentValues> mList) {
        this.mList = mList;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return mList.indexOf(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.kinds_listview_item, null);
            viewHolder = new ViewHolder((ImageView) convertView.findViewById(R.id.imKindsImage), (TextView) convertView
                    .findViewById(R.id.txtKindsName));
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        ContentValues cv = (ContentValues) getItem(position);
        viewHolder.textView.setText(cv.getAsString(KindsDao.COLUMN_KindsName));
        viewHolder.imageView.setImageResource(cv.getAsInteger(KindsDao.COLUMN_ImageResourceID));
        viewHolder.imageView.setTag(cv.getAsInteger(KindsDao.COLUMN_ImageResourceID));

        return convertView;
    }

    private class ViewHolder {
        ImageView imageView;
        TextView textView;

        public ViewHolder(ImageView kindsImage, TextView kindsName) {
            this.imageView = kindsImage;
            this.textView = kindsName;
        }
    }

    public void refresh(List<ContentValues> mList){
        this.mList =mList;
        notifyDataSetChanged();
    }
}
