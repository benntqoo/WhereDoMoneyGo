package com.jrtou.moneygone.Tool.SQLite.Realization;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.jrtou.moneygone.Tool.SQLite.DAO;
import com.jrtou.moneygone.Tool.SQLite.IDAO;

/**
 * Created by Administrator on 2016/11/22.
 *
 *
 */

public class OrderDao extends DAO implements IDAO {
    /*****************************
     * TABLE: 訂單
     * CREATE: 2016/11/07
     * VERSION: 1
     * MEMO:
     *****************************/
    public static final String TABLE_ORDER = "Order";
    public static final String COLUMN_id = "_id";
    public static final String COLUMN_Personal_id = "Personal_id";
    public static final String COLUMN_KindsItems_id = "KindsItems_id";
    public static final String COLUMN_Description = "Description";
    public static final String COLUMN_Price = "Price";
    public static final String COLUMN_Date = "Date";
    public static final String COLUMN_MainCurrency = "MainMoneyType";
    public static final String COLUMN_Currency = "MoneyType";
    public static final String COLUMN_ExchangeRate = "ExchangeRate";
    public static final String COLUMN_Def1 = "def1";
    public static final String COLUMN_Def2 = "def2";

    public static final String CREATE_Order_Table = "CREATE TABLE IF NOT EXISTS " + TABLE_ORDER + "(" +
            COLUMN_id + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +            //PK
            COLUMN_Personal_id + " TEXT NOT NULL," +                                //帳戶編號
            COLUMN_KindsItems_id + " INTEGER NOT NULL," +                           //分類ID
            COLUMN_Price + " REAL NOT NULL," +                                      //消費金額
            COLUMN_Description + " TEXT," +                                         //說明
            COLUMN_Date + " TEXT NOT NULL," +                                       //消費日期 1991/03/25
            COLUMN_MainCurrency +" TEXT NOT NULL,"+                                 //主要貨幣
            COLUMN_Currency + " TEXT," +                                            //消費貨幣別
            COLUMN_ExchangeRate + " REAL," +                                        //主要貨幣與消費貨幣匯率
            COLUMN_Def1 + " TEXT," +
            COLUMN_Def2 + " TEXT);";


    public OrderDao(Context context) {
        super(context);
        initialDetail();
    }

    public OrderDao(SQLiteDatabase sqLiteDatabase) {
        super(sqLiteDatabase);
        initialDetail();
    }

    @Override
    public void onDropTable() {

    }

    @Override
    public void onCreateTable() {

    }

    @Override
    public void onUpgradeTable() {

    }

    @Override
    public void onBuildDetail() {

    }

    @Override
    public void initialDetail() {

    }
}
