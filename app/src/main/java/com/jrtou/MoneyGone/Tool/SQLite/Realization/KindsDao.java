package com.jrtou.moneygone.Tool.SQLite.Realization;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.jrtou.moneygone.Tool.SQLite.DAO;
import com.jrtou.moneygone.Tool.SQLite.IDAO;
import com.jrtou.moneygone.R;


/**
 * Created by Administrator on 2016/11/9.
 */

public class KindsDao extends DAO implements IDAO {
    /*****************************
     * TABLE: 大項
     * CREATE: 2016/11/07
     * VERSION: 1
     * MEMO:
     *****************************/
    private static final String TAG = "KindsDao";

    public static final String TABLE_NAME = "Kinds";                               //表名

    public static final String COLUMN_id = "_id";                                  //PK
    public static final String COLUMN_KindsName = "KindsName";                     //分類名稱
    public static final String COLUMN_MoneyType = "MoneyType";                     //1:支出/0:收入
    public static final String COLUMN_ImageResourceID = "ImageResourceID";         //圖片 Default:0
    public static final String COLUME_Index = "ItemIndex";                             //排序
    public static final String COLUMN_Def1 = "Def1";                               //備用1
    public static final String COLUMN_Def2 = "Def2";                               //備用2

    //建表
    public static final String CREATE_Kinds_Table = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
            COLUMN_id + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            COLUMN_KindsName + " TEXT NOT NULL," +
            COLUMN_MoneyType + " INTEGER NOT NULL, " +
            COLUMN_ImageResourceID + " TEXT NOT NULL DEFAULT 0," +
            COLUME_Index + "INTEGER NOT NULL DEFAULT 0," +
            COLUMN_Def1 + " TEXT," +
            COLUMN_Def2 + " TEXT);";
    //刪表
    public static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;


    public KindsDao(Context context) {
        super(context);
        initialDetail();

    }

    public KindsDao(SQLiteDatabase sqLiteDatabase) {
        super(sqLiteDatabase);
        initialDetail();
    }

    public void initialDetail() {
        Log.wtf(TAG, "initialDetail");
        setTableName(TABLE_NAME);
        setCreateTable(CREATE_Kinds_Table);
        setDropTable(DROP_TABLE);
        setKeyId(COLUMN_id);
    }

    @Override // Delete table
    public void onDropTable() {
        Log.wtf(TAG, "onDropTable: ");
        db.execSQL(getDropTable());
    }

    @Override // Modify schema rebuild schema
    public void onUpgradeTable() {
        Log.wtf(TAG, "onUpgradeTable: ");
    }

    @Override
    public void onCreateTable() {
        Log.wtf(TAG, "onCreateTable: ");
        db.execSQL(getCreateTable());
    }

    @Override //  Insert default  value
    public void onBuildDetail() {
        Log.wtf(TAG, "onBuildDetail: ");

        db.execSQL("INSERT INTO " + TABLE_NAME + " (" + COLUMN_KindsName + "," + COLUMN_MoneyType + "," +
                COLUMN_ImageResourceID + ") VALUES " +
                "('早餐'," + 1 + "," + R.mipmap.item_bread + ");");

        db.execSQL("INSERT INTO " + TABLE_NAME + " (" + COLUMN_KindsName + "," + COLUMN_MoneyType + "," +
                COLUMN_ImageResourceID + ") VALUES " +
                "('午餐'," + 1 + "," + R.mipmap.item_noodles + ");");

        db.execSQL("INSERT INTO " + TABLE_NAME + " (" + COLUMN_KindsName + "," + COLUMN_MoneyType + "," +
                COLUMN_ImageResourceID + ") VALUES " +
                "('晚餐'," + 1 + "," + R.mipmap.item_rice + ");");

        db.execSQL("INSERT INTO " + TABLE_NAME + " (" + COLUMN_KindsName + "," + COLUMN_MoneyType + "," +
                COLUMN_ImageResourceID + ") VALUES " +
                "('飲料'," + 1 + "," + R.mipmap.item_cocktail + ");");

        db.execSQL("INSERT INTO " + TABLE_NAME + " (" + COLUMN_KindsName + "," + COLUMN_MoneyType + "," +
                COLUMN_ImageResourceID + ") VALUES " +
                "('電費'," + 1 + "," + R.mipmap.item_electricity + ");");

        db.execSQL("INSERT INTO " + TABLE_NAME + " (" + COLUMN_KindsName + "," + COLUMN_MoneyType + "," +
                COLUMN_ImageResourceID + ") VALUES " +
                "('水費'," + 1 + "," + R.mipmap.item_water + ");");

        db.execSQL("INSERT INTO " + TABLE_NAME + " (" + COLUMN_KindsName + "," + COLUMN_MoneyType + "," +
                COLUMN_ImageResourceID + ") VALUES " +
                "('服飾'," + 1 + "," + R.mipmap.item_tshit + ");");

        db.execSQL("INSERT INTO " + TABLE_NAME + " (" + COLUMN_KindsName + "," + COLUMN_MoneyType + "," +
                COLUMN_ImageResourceID + ") VALUES " +
                "('加油'," + 1 + "," + R.mipmap.item_gas + ");");

        db.execSQL("INSERT INTO " + TABLE_NAME + " (" + COLUMN_KindsName + "," + COLUMN_MoneyType + "," +
                COLUMN_ImageResourceID + ") VALUES " +
                "('房租'," + 1 + "," + R.mipmap.item_doghouse + ");");

        db.execSQL("INSERT INTO " + TABLE_NAME + " (" + COLUMN_KindsName + "," + COLUMN_MoneyType + "," +
                COLUMN_ImageResourceID + ") VALUES " +
                "('捷運'," + 1 + "," + R.mipmap.item_mrt + ");");

        db.execSQL("INSERT INTO " + TABLE_NAME + " (" + COLUMN_KindsName + "," + COLUMN_MoneyType + "," +
                COLUMN_ImageResourceID + ") VALUES " +
                "('計程車'," + 1 + "," + R.mipmap.item_taxi + ");");

        db.execSQL("INSERT INTO " + TABLE_NAME + " (" + COLUMN_KindsName + "," + COLUMN_MoneyType + "," +
                COLUMN_ImageResourceID + ") VALUES " +
                "('停車費'," + 1 + "," + R.mipmap.item_parking + ");");

        db.execSQL("INSERT INTO " + TABLE_NAME + " (" + COLUMN_KindsName + "," + COLUMN_MoneyType + "," +
                COLUMN_ImageResourceID + ") VALUES " +
                "('交際'," + 1 + "," + R.mipmap.item_strike + ");");

        db.execSQL("INSERT INTO " + TABLE_NAME + " (" + COLUMN_KindsName + "," + COLUMN_MoneyType + "," +
                COLUMN_ImageResourceID + ") VALUES " +
                "('娛樂'," + 1 + "," + R.mipmap.item_game + ");");

        db.execSQL("INSERT INTO " + TABLE_NAME + " (" + COLUMN_KindsName + "," + COLUMN_MoneyType + "," +
                COLUMN_ImageResourceID + ") VALUES " +
                "('電影'," + 1 + "," + R.mipmap.item_film + ");");

        db.execSQL("INSERT INTO " + TABLE_NAME + " (" + COLUMN_KindsName + "," + COLUMN_MoneyType + "," +
                COLUMN_ImageResourceID + ") VALUES " +
                "('罰單'," + 1 + "," + R.mipmap.item_trunk + ");");

        db.execSQL("INSERT INTO " + TABLE_NAME + " (" + COLUMN_KindsName + "," + COLUMN_MoneyType + "," +
                COLUMN_ImageResourceID + ") VALUES " +
                "('信用卡'," + 1 + "," + R.mipmap.item_visa + ");");

        db.execSQL("INSERT INTO " + TABLE_NAME + " (" + COLUMN_KindsName + "," + COLUMN_MoneyType + "," +
                COLUMN_ImageResourceID + ") VALUES " +
                "('生活用品'," + 1 + "," + R.mipmap.item_shopping + ");");

        db.execSQL("INSERT INTO " + TABLE_NAME + " (" + COLUMN_KindsName + "," + COLUMN_MoneyType + "," +
                COLUMN_ImageResourceID + ") VALUES " +
                "('投資'," + 1 + "," + R.mipmap.item_bullish + ");");

        db.execSQL("INSERT INTO " + TABLE_NAME + " (" + COLUMN_KindsName + "," + COLUMN_MoneyType + "," +
                COLUMN_ImageResourceID + ") VALUES " +
                "('聚餐'," + 1 + "," + R.mipmap.item_restaurant + ");");

        db.execSQL("INSERT INTO " + TABLE_NAME + " (" + COLUMN_KindsName + "," + COLUMN_MoneyType + "," +
                COLUMN_ImageResourceID + ") VALUES " +
                "('約會'," + 1 + "," + R.mipmap.item_date + ");");

        db.execSQL("INSERT INTO " + TABLE_NAME + " (" + COLUMN_KindsName + "," + COLUMN_MoneyType + "," +
                COLUMN_ImageResourceID + ") VALUES " +
                "('醫療'," + 1 + "," + R.mipmap.item_hospital + ");");


    }

}
