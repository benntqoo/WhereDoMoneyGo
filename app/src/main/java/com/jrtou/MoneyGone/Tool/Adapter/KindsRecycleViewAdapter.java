package com.jrtou.moneygone.Tool.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jrtou.moneygone.R;
import com.jrtou.moneygone.Tool.ImageTool.InitializeImage;

import java.util.List;

/**
 * Created by Administrator on 2016/11/12.
 *
 */

public class KindsRecycleViewAdapter extends RecyclerView.Adapter<KindsRecycleViewAdapter.ImageViewHolder> {
    private static final String TAG = "RecycleView ItemClick";
    private List<String> mImageIDList;
    private ImageView mImageView;//recycle item
    private ImageView activityImageView;
    private int imageID;

    public KindsRecycleViewAdapter(List<String> mImageIDList, Context mContext, ImageView activityImageView) {
        this.mImageIDList = mImageIDList;
        this.activityImageView = activityImageView;

        mImageView = new ImageView(mContext);
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.kinds_recycleview_image_items, parent,
                false);
        return new ImageViewHolder(v);
    }

    @Override // 設置Item 內容
    public void onBindViewHolder(final ImageViewHolder holder, int position) {
        if (position < InitializeImage.getLength()) {
            final int tImageID;

            tImageID = InitializeImage.images[position];
            holder.mImageView.setImageResource(tImageID);
            holder.mImageView.setTag(tImageID);
            holder.mImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    activityImageView.setImageResource((Integer) holder.mImageView.getTag());
                    imageID = (int) holder.mImageView.getTag();
                    mImageView.setImageDrawable(holder.mImageView.getDrawable());
                    Log.wtf(TAG, "onBindViewHolder: " + tImageID);
                }
            });
        }
    }

    @Override //回傳數量
    public int getItemCount() {
        return mImageIDList.size();
    }

    /**
     * @return 目前圖片ID
     */
    public int getImageID() {
        return imageID;
    }

    /**
     * @param activityImageID 當前資料庫imageID
     */
    public void initImageID(int activityImageID) {
        imageID = activityImageID;
    }

    class ImageViewHolder extends RecyclerView.ViewHolder {
        private ImageView mImageView;

        ImageViewHolder(View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.imageView1);
        }
    }


}
