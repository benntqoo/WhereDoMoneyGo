package com.jrtou.moneygone.Tool.SQLite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/11/9.
 */

public class DAO {
    private static final String TAG = "DAO";
    private static String TABLE_NAME;  // 表格名稱
    private static String KEY_ID;// 編號表格欄位名稱，固定不變
    private static String CREATE_TABLE;// 使用上面宣告的變數建立表格的SQL指令  TEXT   INTEGER    REAL
    private static String DROP_TABLE; //刪除資料表
    public SQLiteDatabase db;// 資料庫物件

    // 建構子，一般的應用都不需要修改
    public DAO(Context context) {
        db = MyDBHelper.getDatabase(context);
    }

    public DAO(SQLiteDatabase sqLiteDatabase) {
        db = sqLiteDatabase;
    }

    public static String getTableName() {
        return TABLE_NAME;
    }

    public static void setTableName(String tableName) {
        TABLE_NAME = tableName;
    }

    public static String getKeyId() {
        return KEY_ID;
    }

    public static void setKeyId(String keyId) {
        KEY_ID = keyId;
    }

    public static String getCreateTable() {
        return CREATE_TABLE;
    }

    public static void setCreateTable(String createTable) {
        CREATE_TABLE = createTable;
    }

    public static String getDropTable() {
        return DROP_TABLE;
    }

    public static void setDropTable(String dropTable) {
        DROP_TABLE = dropTable;
    }

    // 關閉資料庫，一般的應用都不需要修改
    public void close() {
        if (db != null) {
            db.close();
        }
    }

    public boolean insert(ContentValues contentValues) {//新增
        return db.insert(getTableName(), null, contentValues) > 0;
    }

    /*
        // 新增參數指定的物件
        public long insert(ContentValues cv) {
            long id = db.insert(getTableName(), null, cv);
            return id;
        }
    */
    // 修改參數指定的物件
    public boolean modify(ContentValues cv) {
        String where = getKeyId() + "=" + cv.getAsString(getKeyId());
        Log.wtf(TAG, "modify: " + "Update " + getTableName() + " set " + cv + " where" + where);
        // 執行修改資料並回傳修改的資料數量是否成功
        return db.update(getTableName(), cv, where, null) > 0;
    }

    // 刪除參數指定編號的資料
    public boolean delete(String[] _id) {
        Log.wtf(TAG, "delete: ID:" + _id[0]);
        return db.delete(getTableName(), "_id = ?", _id) > 0;
    }

    // 讀取SQLCMD資料
    public List<ContentValues> query(String sql) {
        Log.wtf(TAG, "Query SQL:" + sql);
        ContentValues cv;
        List<ContentValues> result = new ArrayList<ContentValues>();
        Cursor cursor = db.rawQuery(sql, null);

        while (cursor.moveToNext()) {
            cv = new ContentValues();
            DatabaseUtils.cursorRowToContentValues(cursor, cv);
            result.add(cv);
        }
        cursor.close();
        return result;
    }


    // 讀取所有記事資料
    public List<ContentValues> getAll() {
        ContentValues cv;
        List<ContentValues> result = new ArrayList<ContentValues>();
        Cursor cursor = db.query(getTableName(), null, null, null, null, null, null, null);
        while (cursor.moveToNext()) {
            cv = new ContentValues();
            DatabaseUtils.cursorRowToContentValues(cursor, cv);
            result.add(cv);
        }
        cursor.close();
        return result;
    }

    // 取得指定編號的資料物件
    public ContentValues get(long id) {
        Cursor cursor = null;  // 準備回傳結果用的物件
        ContentValues cv = new ContentValues();
        String where = getKeyId() + "=" + id; // 使用編號為查詢條件
        // 執行查詢
        Cursor result = db.query(getTableName(), null, where, null, null, null, null, null);
        // 如果有查詢結果
        if (result.moveToFirst()) {
            // 讀取包裝一筆資料的物件
            DatabaseUtils.cursorRowToContentValues(result, cv);
        }
        // 關閉Cursor物件
        result.close();
        // 回傳結果
        return cv;
    }

    // 取得資料數量
    public int getCount() {
        int result = 0;
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + getTableName(), null);
        if (cursor.moveToNext()) {
            result = cursor.getInt(0);
        }
        cursor.close();
        return result;
    }
}
