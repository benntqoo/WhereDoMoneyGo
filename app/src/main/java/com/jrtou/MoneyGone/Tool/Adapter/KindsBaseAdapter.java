package com.jrtou.moneygone.Tool.Adapter;

import android.content.ContentValues;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.jrtou.moneygone.R;

import java.util.List;

/**
 * Created by Administrator on 2016/11/10.
 */

public class KindsBaseAdapter extends BaseAdapter {
    private static final String TAG = "KindsBaseAdapter";
    LayoutInflater mLayoutInflater;
    List<ContentValues> mList;

    public KindsBaseAdapter(Context context, List<ContentValues> mList) {
        this.mLayoutInflater = LayoutInflater.from(context);
        this.mList = mList;
    }

    @Override//回傳總數
    public int getCount() {
        return mList.size();
    }

    @Override//取得contentValues 物件
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override//取得物件ID
    public long getItemId(int position) {
        return mList.indexOf(getItem(position));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.wtf(TAG, "getView: ");
        ViewHolder viewHolder = null;

        if (convertView == null) {
            convertView = mLayoutInflater.inflate(R.layout.kinds_listview_item, null);
            viewHolder = new ViewHolder(
                    (ImageView) convertView.findViewById(R.id.imKindsImage),
                    (TextView) convertView.findViewById(R.id.txtKindsName));

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Log.wtf(TAG, "listView" + mList);
        Log.wtf(TAG, "getCount:" + getCount() + " getItem:" + getItem(position) + " Position:" + position);
        ContentValues cv = (ContentValues) getItem(position);
        //** _id , KindsName , ImageResourceID , def1 , def2
        Log.wtf(TAG, "getView: ImageResourceID:" + cv.getAsString("ImageResourceID") + " KindsName:" + cv.getAsString
                ("KindsName"));
        String tImageSrc = cv.getAsString("ImageResourceID");

        int src;
        if (!tImageSrc.equals("0") || !tImageSrc.equals("")) {
            src = cv.getAsInteger("ImageResourceID");
            Log.wtf(TAG, "getImageID: " + src);
            viewHolder.mImage.setImageResource(src);
        } else {
            src = R.drawable.ic_android_black_24dp;
            viewHolder.mImage.setImageResource(src);
        }

        viewHolder.mText.setText(cv.getAsString("KindsName"));


        return convertView;
    }

    private class ViewHolder {
        ImageView mImage;
        TextView mText;

        public ViewHolder(ImageView mImage, TextView mText) {
            this.mImage = mImage;
            this.mText = mText;
        }
    }
}
