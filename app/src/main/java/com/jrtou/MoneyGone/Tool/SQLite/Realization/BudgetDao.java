package com.jrtou.moneygone.Tool.SQLite.Realization;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.jrtou.moneygone.Tool.SQLite.DAO;
import com.jrtou.moneygone.Tool.SQLite.IDAO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/11/18.
 *
 */

public class BudgetDao extends DAO implements IDAO {

    /*****************************
     * TABLE: 預算表
     * CREATE: 2016/11/18
     * VERSION: 1
     * MEMO:
     *****************************/
    private static final String TAG = "BudgetDao";

    public static final String TABLE_NAME = "Budget";                               //表名

    public static final String COLUMN_id = "_id";                                   //PK
    public static final String COLUMN_Amount = "Amount";                            //固定金額 Default:-1
    public static final String COLUMN_OtherAmount = "OtherAmount";                  //獨立金額
    public static final String COLUMN_YearMonth = "YearMonth";                      //年月 201611
    public static final String COLUMN_COST = "Cost";                                //已經消費
    public static final String COLUMN_SURPLUS = "Surplus";                          //剩餘金額
    public static final String COLUMN_Currency = "Currency";                        //貨幣別
    public static final String COLUMN_Def1 = "Def1";                                //備用1
    public static final String COLUMN_Def2 = "Def2";                                //備用2


    private static final String CREATE_Budget_Table = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "(" +
            COLUMN_id + " INTEGER PRIMARY KEY AUTOINCREMENT," +
            COLUMN_Amount + " REAL NOT NULL," +
            COLUMN_OtherAmount + " REAL DEFAULT -1," +
            COLUMN_YearMonth + " TEXT(10) NOT NULL," +
            COLUMN_COST + " REAL NOT NULL DEFAULT 0," +
            COLUMN_SURPLUS + " REAL NOT NULL," +
            COLUMN_Currency + " TEXT NOT NULL," +
            COLUMN_Def1 + " TEXT," +
            COLUMN_Def2 + " TEXT);";

    private static final String DROP_TABLE = "DROP TABLE IF EXISTS " + TABLE_NAME;


    @Override
    public void initialDetail() {
        Log.wtf(TAG, "BudgetDao initialDetail");
        setTableName(TABLE_NAME);
        setCreateTable(CREATE_Budget_Table);
        setDropTable(DROP_TABLE);
        setKeyId(COLUMN_id);
    }

    public BudgetDao(Context context) {
        super(context);
        initialDetail();
    }

    public BudgetDao(SQLiteDatabase sqLiteDatabase) {
        super(sqLiteDatabase);
        initialDetail();
    }

    @Override
    public List<ContentValues> getAll() {
        ContentValues cv;
        List<ContentValues> result = new ArrayList<ContentValues>();
        Cursor cursor = db.query(getTableName(), null, null, null, null, null, BudgetDao.COLUMN_id + " DESC", null);
        while (cursor.moveToNext()) {
            cv = new ContentValues();
            DatabaseUtils.cursorRowToContentValues(cursor, cv);
            result.add(cv);
        }
        cursor.close();
        return result;
    }

    @Override
    public void onDropTable() {
        Log.wtf(TAG, "onDropTable: " + getDropTable());
        db.execSQL(getDropTable());
    }

    @Override
    public void onCreateTable() {
        Log.wtf(TAG, "onCreateTable: " + getCreateTable());
        db.execSQL(getCreateTable());
    }

    @Override
    public void onUpgradeTable() {

    }

    @Override
    public void onBuildDetail() {
        Log.wtf(TAG, "onBuildDetail: ");
        db.execSQL("INSERT INTO " + TABLE_NAME + " (" +
                COLUMN_Amount + "," +
                COLUMN_OtherAmount + "," +
                COLUMN_YearMonth + "," +
                COLUMN_COST + "," +
                COLUMN_SURPLUS + "," +
                COLUMN_Currency +
                ") VALUES " +
                "(" + 10000 + "," + -1 + ",'2016/01'," + 1000 + "," + 9000 + ",'TW');");
        db.execSQL("INSERT INTO " + TABLE_NAME + " (" +
                COLUMN_Amount + "," +
                COLUMN_OtherAmount + "," +
                COLUMN_YearMonth + "," +
                COLUMN_COST + "," +
                COLUMN_SURPLUS + "," +
                COLUMN_Currency +
                ") VALUES " +
                "(" + 10000 + "," + -1 + ",'2016/02'," + 5000 + "," + 5000 + ",'TW');");
        db.execSQL("INSERT INTO " + TABLE_NAME + " (" +
                COLUMN_Amount + "," +
                COLUMN_OtherAmount + "," +
                COLUMN_YearMonth + "," +
                COLUMN_COST + "," +
                COLUMN_SURPLUS + "," +
                COLUMN_Currency +
                ") VALUES " +
                "(" + 10000 + "," + -1 + ",'2016/03'," + 4000 + "," + 6000 + ",'TW');");
        db.execSQL("INSERT INTO " + TABLE_NAME + " (" +
                COLUMN_Amount + "," +
                COLUMN_OtherAmount + "," +
                COLUMN_YearMonth + "," +
                COLUMN_COST + "," +
                COLUMN_SURPLUS + "," +
                COLUMN_Currency +
                ") VALUES " +
                "(" + 10000 + "," + -1 + ",'2016/04'," + 7000 + "," + 3000 + ",'TW');");
        db.execSQL("INSERT INTO " + TABLE_NAME + " (" +
                COLUMN_Amount + "," +
                COLUMN_OtherAmount + "," +
                COLUMN_YearMonth + "," +
                COLUMN_COST + "," +
                COLUMN_SURPLUS + "," +
                COLUMN_Currency +
                ") VALUES " +
                "(" + 10000 + "," + -1 + ",'2016/05'," + 1000 + "," + 9000 + ",'TW');");
        db.execSQL("INSERT INTO " + TABLE_NAME + " (" +
                COLUMN_Amount + "," +
                COLUMN_OtherAmount + "," +
                COLUMN_YearMonth + "," +
                COLUMN_COST + "," +
                COLUMN_SURPLUS + "," +
                COLUMN_Currency +
                ") VALUES " +
                "(" + 10000 + "," + -1 + ",'2016/06'," + 7000 + "," + 3000 + ",'TW');");
        db.execSQL("INSERT INTO " + TABLE_NAME + " (" +
                COLUMN_Amount + "," +
                COLUMN_OtherAmount + "," +
                COLUMN_YearMonth + "," +
                COLUMN_COST + "," +
                COLUMN_SURPLUS + "," +
                COLUMN_Currency +
                ") VALUES " +
                "(" + 10000 + "," + -1 + ",'2016/07'," + 10000 + "," + 0 + ",'TW');");
        db.execSQL("INSERT INTO " + TABLE_NAME + " (" +
                COLUMN_Amount + "," +
                COLUMN_OtherAmount + "," +
                COLUMN_YearMonth + "," +
                COLUMN_COST + "," +
                COLUMN_SURPLUS + "," +
                COLUMN_Currency +
                ") VALUES " +
                "(" + 10000 + "," + -1 + ",'2016/08'," + 8000 + "," + 2000 + ",'TW');");
        db.execSQL("INSERT INTO " + TABLE_NAME + " (" +
                COLUMN_Amount + "," +
                COLUMN_OtherAmount + "," +
                COLUMN_YearMonth + "," +
                COLUMN_COST + "," +
                COLUMN_SURPLUS + "," +
                COLUMN_Currency +
                ") VALUES " +
                "(" + 10000 + "," + -1 + ",'2016/09'," + 11000 + "," + -1000 + ",'TW');");
        db.execSQL("INSERT INTO " + TABLE_NAME + " (" +
                COLUMN_Amount + "," +
                COLUMN_OtherAmount + "," +
                COLUMN_YearMonth + "," +
                COLUMN_COST + "," +
                COLUMN_SURPLUS + "," +
                COLUMN_Currency +
                ") VALUES " +
                "(" + 10000 + "," + -1 + ",'2016/10'," + 1000 + "," + 9000 + ",'TW');");
        db.execSQL("INSERT INTO " + TABLE_NAME + " (" +
                COLUMN_Amount + "," +
                COLUMN_OtherAmount + "," +
                COLUMN_YearMonth + "," +
                COLUMN_COST + "," +
                COLUMN_SURPLUS + "," +
                COLUMN_Currency +
                ") VALUES " +
                "(" + 10000 + "," + -1 + ",'2016/11'," + 15000 + "," + -5000 + ",'TW');");
        db.execSQL("INSERT INTO " + TABLE_NAME + " (" +
                COLUMN_Amount + "," +
                COLUMN_OtherAmount + "," +
                COLUMN_YearMonth + "," +
                COLUMN_COST + "," +
                COLUMN_SURPLUS + "," +
                COLUMN_Currency +
                ") VALUES " +
                "(" + 10000 + "," + -1 + ",'2016/12'," + 1000 + "," + 9000 + ",'TW');");
    }


}
